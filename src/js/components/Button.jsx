import styles from "../../style/components/Button.module.scss"

const Button = ({clickFn, children}) => {
  return (
    <button className={styles.Button} onClick={clickFn}>
      {children}
    </button>
  )
}

export default Button;