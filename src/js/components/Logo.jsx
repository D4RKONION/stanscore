import { Link } from "react-router-dom";
import styles from "../../style/components/Logo.module.scss"

const Logo = () => {
  return(
    <Link to="/home"><h1 className={styles.Logo}>stanScore</h1></Link>
  )
}
  

export default Logo;