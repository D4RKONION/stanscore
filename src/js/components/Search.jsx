import styles from '../../style/components/Search.module.scss'

const Search = ({ value, onChange, placeholder, children }) =>
  <form className={styles.Search} onSubmit={(event) =>  event.preventDefault()}>
    {children} <input
      type="text"
      value={value} 
      onChange={onChange}
      placeholder={placeholder}
    />
  </form>

export default Search;