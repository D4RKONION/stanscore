const PERCENTANDSTENS = {
     ">130": {
      "percentile": 99,
      "STEN": 10
    },
    "130": {
      "percentile": 98,
      "STEN": 10
    },
    "129": {
      "percentile": 97,
      "STEN": 9
    },
    "128": {
      "percentile": 97,
      "STEN": 9
    },
    "127": {
      "percentile": 96,
      "STEN": 9
    },
    "126": {
      "percentile": 96,
      "STEN": 9
    },
    "125": {
      "percentile": 95,
      "STEN": 9
    },
    "124": {
      "percentile": 95,
      "STEN": 9
    },
    "123": {
      "percentile": 94,
      "STEN": 9
    },
    "122": {
      "percentile": 93,
      "STEN": 8
    },
    "121": {
      "percentile": 92,
      "STEN": 8
    },
    "120": {
      "percentile": 91,
      "STEN": 8
    },
    "119": {
      "percentile": 90,
      "STEN": 8
    },
    "118": {
      "percentile": 88,
      "STEN": 8
    },
    "117": {
      "percentile": 87,
      "STEN": 8
    },
    "116": {
      "percentile": 86,
      "STEN": 8
    },
    "115": {
      "percentile": 84,
      "STEN": 7
    },
    "114": {
      "percentile": 82,
      "STEN": 7
    },
    "113": {
      "percentile": 81,
      "STEN": 7
    },
    "112": {
      "percentile": 79,
      "STEN": 7
    },
    "111": {
      "percentile": 77,
      "STEN": 7
    },
    "110": {
      "percentile": 75,
      "STEN": 7
    },
    "109": {
      "percentile": 73,
      "STEN": 7
    },
    "108": {
      "percentile": 70,
      "STEN": 7
    },
    "107": {
      "percentile": 68,
      "STEN": 6
    },
    "106": {
      "percentile": 66,
      "STEN": 6
    },
    "105": {
      "percentile": 63,
      "STEN": 6
    },
    "104": {
      "percentile": 61,
      "STEN": 6
    },
    "103": {
      "percentile": 58,
      "STEN": 6
    },
    "102": {
      "percentile": 55,
      "STEN": 6
    },
    "101": {
      "percentile": 53,
      "STEN": 6
    },
    "100": {
      "percentile": 50,
      "STEN": 6
    },
    "99": {
      "percentile": 47,
      "STEN": 5
    },
    "98": {
      "percentile": 45,
      "STEN": 5
    },
    "97": {
      "percentile": 42,
      "STEN": 5
    },
    "96": {
      "percentile": 39,
      "STEN": 5
    },
    "95": {
      "percentile": 37,
      "STEN": 5
    },
    "94": {
      "percentile": 34,
      "STEN": 5
    },
    "93": {
      "percentile": 32,
      "STEN": 5
    },
    "92": {
      "percentile": 30,
      "STEN": 4
    },
    "91": {
      "percentile": 27,
      "STEN": 4
    },
    "90": {
      "percentile": 25,
      "STEN": 4
    },
    "89": {
      "percentile": 23,
      "STEN": 4
    },
    "88": {
      "percentile": 21,
      "STEN": 4
    },
    "87": {
      "percentile": 19,
      "STEN": 4
    },
    "86": {
      "percentile": 18,
      "STEN": 4
    },
    "85": {
      "percentile": 16,
      "STEN": 4
    },
    "84": {
      "percentile": 14,
      "STEN": 3
    },
    "83": {
      "percentile": 13,
      "STEN": 3
    },
    "82": {
      "percentile": 12,
      "STEN": 3
    },
    "81": {
      "percentile": 10,
      "STEN": 3
    },
    "80": {
      "percentile": 9,
      "STEN": 3
    },
    "79": {
      "percentile": 8,
      "STEN": 3
    },
    "78": {
      "percentile": 7,
      "STEN": 3
    },
    "77": {
      "percentile": 6,
      "STEN": 3
    },
    "76": {
      "percentile": 5,
      "STEN": 2
    },
    "75": {
      "percentile": 5,
      "STEN": 2
    },
    "74": {
      "percentile": 4,
      "STEN": 2
    },
    "73": {
      "percentile": 4,
      "STEN": 2
    },
    "72": {
      "percentile": 3,
      "STEN": 2
    },
    "71": {
      "percentile": 3,
      "STEN": 2
    },
    "70": {
      "percentile": 2,
      "STEN": 1
    },
    "<70": {
      "percentile": 1,
      "STEN": 1
    }
}
    
export default PERCENTANDSTENS