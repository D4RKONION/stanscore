import { useState } from "react";
import { useParams } from "react-router-dom";
import Search from "../components/Search";
import TESTS from "../constants/Tests";
import PERCENTANDSTENS from "../constants/PercentAndStens";
import "../../style/pages/Standardise.scss";

const Standardise = () => {

  const testName = useParams().testName;
  const testClass = useParams().testClass;
  const testPart = useParams().testPart;

  const [form, setForm] = useState("");
  const [score, setScore] = useState("");

  return(
    <div className="Standardise">
      <h1>{testName}</h1>
      <h2>{testClass} Class - {testPart}</h2>

      <div className="tipBox">
        <p><i>"20" will give you both forms</i></p>
        <p><i>"A20" will give you form A</i></p>
        <p><i>"B20" will give you form B</i></p>
        <hr></hr>
        <p><i>STEN/Percentile? Use A or B</i></p>
      </div>        

      <Search
        value={form + score}
        placeholder="Raw Score goes here"
        onChange={e => {
          if (e.target.value[0] === "A" || e.target.value[0] === "B") {
            setForm(e.target.value[0]);
            setScore(e.target.value.substring(1));
          } else {
            setForm("")
            setScore(e.target.value)
          }
        }}     
      ></Search>

      {TESTS[testName][testClass][testPart][score] &&
        <table>
          <thead>
            <tr>
              <td>Raw</td>
              {(!form || form !== "B") &&
                <td>Form A</td>
              }
              {(!form || form !== "A") &&
                <td>Form B</td>
              }
              {form && <td>Percentile</td>}
              {form && <td>STEN</td>}
            </tr>
          </thead> 
          <tbody>
            <tr>
              <td>{score}</td>
              {(!form || form !== "B") &&
                <td>{TESTS[testName][testClass][testPart][score]["Form A"]}</td>
              }
              {(!form || form !== "A") &&
                <td>{TESTS[testName][testClass][testPart][score]["Form B"]}</td>
              }
              {form &&
                <td>{PERCENTANDSTENS[TESTS[testName][testClass][testPart][score][`Form ${form}`]]["percentile"]}</td>
              }
              {form &&
                <td>{PERCENTANDSTENS[TESTS[testName][testClass][testPart][score][`Form ${form}`]]["STEN"]}</td>
              }
            </tr>  
          </tbody>
        </table>  
      }


      
    </div>
  )
}

export default Standardise;