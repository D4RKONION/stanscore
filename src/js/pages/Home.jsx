import Button from "../components/Button";
import { useHistory } from "react-router-dom";
import TESTS from "../constants/Tests";
import "../../style/pages/Home.scss"

const Home = () => {

  const history = useHistory();

  return (
    <div className="Home">
      <h2>Select a test to convert</h2>
      {Object.keys(TESTS).map(testName =>
        <div key={`section-${testName}`}>
          <h2>{testName}</h2>
          <div className="testNameGroup">
            {Object.keys(TESTS[testName]).map(testClass =>
              <div key={`classsection-${testClass}`}>
                <h3>{testClass}</h3>
                {Object.keys(TESTS[testName][testClass]).map(testPart => 
                  <Button
                  key={`btn-${testName}-${testClass}-${testPart}`}
                  clickFn={() => history.push(`/${testName}/${testClass}/${testPart}`)}
                >{testPart}</Button>
                )}
              </div> 
            )}
          </div>
        </div>
      )}
    </div>
  )
}

export default Home;