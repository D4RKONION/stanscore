import {
	HashRouter as Router,
	Switch,
	Route,
	Redirect,
} from "react-router-dom";
import './App.css';
import Logo from "./js/components/Logo";
import Home from "./js/pages/Home";
import Standardise from "./js/pages/Standardise";

function App() {
  return (
    <div className="App">
			
      <Router>
			<Logo></Logo>
				<Switch>
          <Route exact path={`/:testName/:testClass/:testPart`}>
						<Standardise />
					</Route>
          <Route path={`/home`}>
						<Home />
					</Route>
					<Redirect from='*' to='/home' />
				</Switch>

			</Router>
    </div>
  );
}

export default App;
